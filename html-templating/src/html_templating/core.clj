(ns html-templating.core
  (:require [selmer.parser :as selmer]
            [selmer.filters :as filters]
            [selmer.middleware :refer [wrap-error-page]]))

(filters/add-filter! :empty? empty?)

(filters/add-filter! :foo
                     (fn [x] [:safe (.toUpperCase x)]))

(selmer/render "{% if files|empty? %}no files{% else %}files{% endif %}"
               {:files []})

(selmer/add-tag!
 :uppercase
 (fn [args context-map content]
   (.toUpperCase (get-in content [:uppercase :content])))
 :enduppercase)

(defn renderer []
  (wrap-error-page
   (fn [template]
     {:status 200
      :body (selmer/render-file template {})})))

;; user=> (use 'selmer.parser)
;; nil

;; user=> (render "Hello, {{name}}" {:name "World"})
;; "Hello, World"
